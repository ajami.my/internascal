import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/Home/HomePage'
import RegisterLoginPage from '@/components/RegisterLogin/RegisterLoginPage.vue'
import LogingPage from '@/components/RegisterLogin/LogingPage.vue'
import ProductInfoPage from '@/components/ProductInfo/ProductInfoPage.vue'
import EditProductInfoPage from '@/components/EditProductInfo/EditProductInfoPage.vue'
import FormProductPage from '@/components/AddProduct/FormProductPage.vue'
import ProductList from '@/components/ProductList/ProductList.vue'
import AddUserPage from '@/components/AddUser/AddUserPage.vue'
import UserList from '@/components/UserList/UserList.vue'
import UserProfile from '@/components/UserProfile/UserProfile.vue'
import MessengerPage from '@/components/Messenger/MessengerPage.vue'
import UserPanelControlPage from '@/components/UserProfile/UserPanelControlPage.vue'
import AboutUs from '@/components/pages/AboutUs.vue'
import PrivacyPolicy from '@/components/pages/PrivacyPolicy.vue'
import UsageRules from '@/components/pages/UsageRules.vue'
import CategoriesConfigurationPage from '@/components/CategoryConfigurationPage/CategoriesConfigurationPage.vue'
import ADashBoardPage from '@/components/DashBoard/ADashBoardPage.vue'
import ProductsManagementPage from '@/components/ProductsManagement/ProductsManagementPage.vue'
import PromosList from '@/components/PromosList/PromosList.vue'
import MyProductsPage from '@/components/MyProducts/MyProductsPage.vue'
//import Header from '@/components/Header/Header.vue'
import App from '@/App.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    { path: '/App/:idUser', name: 'App', component: App },

    { path: '/home/:idActualUser', name: 'home', component: HomePage },
    { path: '/home', name: 'home', component: HomePage },
    { path: '/', name: 'home', component: HomePage },
    {
      path: '/RegisterLoginPage',
      name: 'RegisterLoginPage',
      component: RegisterLoginPage
    },
    { path: '/LogingPage', name: 'LogingPage', component: LogingPage },
    {
      path: '/product-detail/:idActualUser/:id_product',
      name: 'ProductInfoPage',
      component: ProductInfoPage
    },
    {
      path: '/product-detail/:id_product',
      name: 'ProductInfoPage',
      component: ProductInfoPage
    },
    {
      path: '/edit-product-detail/:idActualUser/:idproduct',
      name: 'EditProductInfoPage',
      component: EditProductInfoPage
    },
    {
      path: '/add-product',
      name: 'FormProductPage',
      component: FormProductPage
    },
    {
      path: '/productList/:idActualUser',
      name: 'ProductList',
      component: ProductList
    },
    {
      path: '/get/productList/:categoryName',
      name: 'ProductList',
      component: ProductList
    },
    { path: '/productList', name: 'ProductList', component: ProductList },
    {
      path: '/productList/searchedProductsByName/:name',
      name: 'ProductListOrderByName',
      component: ProductList
    },

    {
      path: '/promosList/:idActualUser',
      name: 'promosList',
      component: PromosList
    },
    {
      path: '/get/promosList/:categoryName',
      name: 'promosList',
      component: PromosList
    },
    { path: '/promosList', name: 'promosList', component: PromosList },
    {
      path: '/promosList/searchedProductsByName/:name',
      name: 'promosListOrderByName',
      component: PromosList
    },

    { path: '/add-user', name: 'AddUserPage', component: AddUserPage },
    { path: '/userList/:idActualUser', name: 'UserList', component: UserList },
    {
      path: '/user-profile/:idActualUser/:id_user',
      name: 'user-profile',
      component: UserProfile
    },
    {
      path: '/MessengerPage/:idActualUser/:id_user',
      name: 'MessengerPage',
      component: MessengerPage
    },
    {
      path: '/UserPanelControlPage/:idActualUser',
      name: 'UserPanelControlPage',
      component: UserPanelControlPage
    },
    { path: '/about-us', name: 'AboutUs', component: AboutUs },
    {
      path: '/privacy-policy',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy
    },

    {
      path: '/MyProduct/MyProductsPage/:idActualUser',
      name: 'MyProductsPage',
      component: MyProductsPage
    },

    { path: '/add-user', name: 'AddUserPage', component: AddUserPage },
    { path: '/userList/:idActualUser', name: 'UserList', component: UserList },
    {
      path: '/user-profile/:idActualUser/:id_user',
      name: 'user-profile',
      component: UserProfile
    },
    {
      path: '/MessengerPage/:idActualUser/:id_user',
      name: 'MessengerPage',
      component: MessengerPage
    },
    {
      path: '/UserPanelControlPage/:idActualUser',
      name: 'UserPanelControlPage',
      component: UserPanelControlPage
    },
    { path: '/about-us', name: 'AboutUs', component: AboutUs },
    {
      path: '/privacy-policy',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy
    },

    {
      path: '/usage-rules',
      name: 'UsageRules',
      component: UsageRules
    },

    {
      path: '/CategoriesConfigurationPage',
      name: 'CategoriesConfigurationPage',
      component: CategoriesConfigurationPage
    },
    {
      path: '/DashBoard/:idActualUser',
      name: 'ADashBoardPage',
      component: ADashBoardPage
    },
    {
      path: '/ProductsManagementPage',
      name: 'ProductsManagementPage',
      component: ProductsManagementPage
    }
  ]
})

export default router
