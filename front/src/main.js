import Vue from 'vue'
import App from './App.vue'
import ImageUploader from 'vue-image-upload-resize'
import router from './routes'
import VueCarousel from 'vue-carousel'
import VueSingleSelect from 'vue-single-select'
import DynamicSelect from 'vue-dynamic-select'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {}

export default new Vuetify(opts)

Vue.use(DynamicSelect)

Vue.component('vue-single-select', VueSingleSelect)

Vue.use(VueCarousel)

Vue.use(ImageUploader)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  render: h => h(App),

  router
}).$mount('#app')
