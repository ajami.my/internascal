import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    primary: '#dfebe8',
    info: '#2c3e50'
  }
})
