import { v4 as uuidv4 } from 'uuid'
const root = '/api'


export default {
  async loadProductList() {
    const result = await fetch(`${root}/products`)
    return await result.json()
  },
  async loadPromoList() {
    const result = await fetch(`${root}/products/promos`)
    return await result.json()
  },

  async loadProductsEnAwait() {
    const result = await fetch(`${root}/products/load-products-en-await`)
    return await result.json()
  },
  async getIdActualUser() {
    const result = await fetch(`${root}/users/get-role/` + localStorage.getItem("idActualUser"))
    return await result.json()
  },

  async loadRejectedProducts() {
    const result = await fetch(`${root}/products/load-rejected-products`)
    return await result.json()
  },

  async loadConfirmedProducts() {
    const result = await fetch(`${root}/products/load-confirmed-products`)
    return await result.json()
  },

  async loadReservedProducts() {
    const result = await fetch(`${root}/products/load-reserved-products`)
    return await result.json()
  },

  async loadProductDetails(idSearched) {
    const result = await fetch(`${root}/products/` + idSearched)
    return await result.json()
  },

  async getMyProductsList() {
    const result = await fetch(`${root}/my_products/` + localStorage.getItem("idActualUser"))
    return await result.json()
  },

  async getMyTransactions(idActualUser) {
    const result = await fetch(`${root}/get-buyed-products/` + idActualUser)
    return await result.json()
  },

  async getMyTransactionsItemReserveds(idActualUser) {
    const result = await fetch(`${root}/get-reserved-products/` + idActualUser)
    return await result.json()
  },

  async getAllMessages(idActualUser) {
    const result = await fetch(`${root}/get-messages/` + idActualUser)
    return await result.json()
  },

  async uploadActualLimitation() {
    const result = await fetch(`${root}/limitationLastProduct`)
    return await result.json()
  },

  async uploadCategoriesData() {
    const result = await fetch(`${root}/products/more-viewed-categories/`)
    return await result.json()
  },

  async deleteCategory(idCategory) {
    const result = await fetch(`${root}/delete/categories/` + idCategory, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return await result.json()
  },

  async loadCategories() {
    const result = await fetch(`${root}/categories`)
    return await result.json()
  },

  async loadCategoriesName() {
    const result = await fetch(`${root}/get-categories-names`)
    return await result.json()
  },

  async loadCategoryDetails(idCategory) {
    const result = await fetch(`${root}/categories/` + idCategory)
    return await result.json()
  },

  async loadProductsDetailsBySearchedCategory(searchedCategory) {
    const result = await fetch(
      `${root}/products/categories/` + searchedCategory
    )
    return await result.json()
  },

  async saveNewProduct(product) {
    const result = await fetch(`${root}/products/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idproduct: uuidv4(),
        name: product.name,
        description: product.description,
        category: product.category,
        price: product.price,
        urlphoto: product.image.dataUrl,
        id_prop: product.id_prop,
        datecreate: product.date_create
      })
    })
    return await result.json()
  },

  async uploadUpdatedProduct(idSearched, localproduct) {
    const result = await fetch(`${root}/products/` + idSearched, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idproduct: localproduct.idproduct,
        name: localproduct.name,
        description: localproduct.description,
        category: localproduct.category,
        price: localproduct.price,
        urlphoto: localproduct.image,
        tag1: localproduct.tag1,
        tag2: localproduct.tag2,
        tag3: localproduct.tag3,
        promos: localproduct.promos,
        new_price: localproduct.new_price,
        datecreate: new Date().toLocaleString()
      })
    })
    return await result.json()
  },

  async savingEditCategoryDetails(idSearched, localCategory) {
    const result = await fetch(`${root}/categories/` + idSearched, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idCategory: localCategory.idCategory,
        categoryName: localCategory.categoryName,
        icons: localCategory.icons
      })
    })
    return await result.json()
  },

  async saveNewCategoryDetails(newCategory) {
    const result = await fetch(`${root}/categories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idCategory: uuidv4(),
        categoryName: newCategory.categoryName,
        icons: newCategory.image.dataUrl
      })
    })
    return await result.json()
  },

  async loadUserList() {
    const result = await fetch(`${root}/users`)
    return await result.json()
  },

  async loadLastProductsList() {
    const result = await fetch(`${root}/last-products/products`)
    return await result.json()
  },

  async LoadMoreViewedProductsList() {
    const result = await fetch(`${root}/more-viewed/products`)
    return await result.json()
  },

  async loadFeaturedProductsList() {
    const result = await fetch(`${root}/featured/products`)
    return await result.json()
  },

  async loadPromosProductsList() {
    const result = await fetch(`${root}/promos/products`)
    return await result.json()
  },

  async searchProduct(searchedWord) {
    const result = await fetch(
      `${root}/search_product/products/` + searchedWord
    )
    return await result.json()
  },
  async searchProductByName(searchedWord) {
    const result = await fetch(
      `${root}/search_product_by_name/products/` + searchedWord
    )
    return await result.json()
  },

  async loadUserRoles() {
    const result = await fetch(`${root}/roles`)
    return await result.json()
  },

  async loadUserDetails(idSearchedUser) {
    const result = await fetch(`${root}/users/` + idSearchedUser)
    return await result.json()
  },

  async uploadUpdatedUser(idSearchedUser, localUser) {
    const result = await fetch(`${root}/users/` + idSearchedUser, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        iduser: localUser.iduser,
        nickname: localUser.nickname,
        role: localUser.role,
        penascales: localUser.penascales,
        urlphoto: localUser.image,
        datecreate: new Date().toLocaleString()
      })
    })
    return await result.json()
  },

  async saveNewUser(user) {
    const result = await fetch(`${root}/users/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        iduser: uuidv4(),
        nickname: user.nickname,
        mail: user.mail,
        password: user.password,
        datecreate: user.date_create
      })
    })
    return await result.json()
  },
  async login(mailLogin, passwordLogin) {
    const result = await fetch(`${root}/log/users/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        mail: mailLogin,
        password: passwordLogin
      })
    })
    return await result.json()
  },

  async sendMessage(message, idSender, idReceiver) {
    const result = await fetch(`${root}/add-messages/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id_message: uuidv4(),
        id_from: idSender,
        id_to: idReceiver,
        message: message
      })
    })
    return await result.json()
  },

  async updateReservationsTable(idproduct, iduser) {
    const result = await fetch(`${root}/reservations`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idReservation: uuidv4(),
        idproduct: idproduct,
        iduser: iduser
      })
    })
    return await result.json()
  },

  async verifySold(idproduct, iduser) {
    const result = await fetch(
      `${root}/soldVerefication/users/` + idproduct + `&` + iduser
    )
    return await result.json()
  },
  async verifySoldForPromo(idproduct, iduser) {
    const result = await fetch(
      `${root}/soldVerefication-for-promo/users/` + idproduct + `&` + iduser
    )
    return await result.json()
  },

  async updatePenascalesAfterReservation(idproduct, iduser) {
    const result = await fetch(
      `${root}/users/updatePenascales/` + idproduct + `&` + iduser,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },


  async changeConfirmedProductStateToDisponible(idproduct) {
    const result = await fetch(
      `${root}/updateState/confirmed-to-stock/products/` + idproduct,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },
  async updatePenascalesAfterPromoReservation(idproduct, iduser) {
    const result = await fetch(
      `${root}/users/updatePenascales-after-reserving-promo/` +
      idproduct +
      `&` +
      iduser,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async updateProductStateAfterReservation(idproduct) {
    const result = await fetch(`${root}/updateState/products/` + idproduct, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return await result.json()
  },
  async changeProductStateToConfirmed(idproduct) {
    const result = await fetch(
      `${root}/updateState/confirm/products/` + idproduct,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async changeProductStateToRejected(idproduct) {
    const result = await fetch(
      `${root}/updateState/reject/products/` + idproduct,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async changeProductStateToDisponible(idproduct, idReservatiopn, id_user) {
    const result = await fetch(
      `${root}/updateState/to-stock/products/` +
      idproduct +
      `&` +
      idReservatiopn +
      `&` +
      id_user,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async changePromoProductStateToDisponible(
    idproduct,
    idReservatiopn,
    id_user
  ) {
    const result = await fetch(
      `${root}/updatePromoState/to-stock/products/` +
      idproduct +
      `&` +
      idReservatiopn +
      `&` +
      id_user,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async changeProductStateToSold(idproduct, idReservatiopn, id_user) {
    const result = await fetch(
      `${root}/updateState/to-selled/products/` +
      idproduct +
      `&` +
      idReservatiopn +
      `&` +
      id_user,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    return await result.json()
  },

  async saveLastProductNewLimitation(newLimitation) {
    const result = await fetch(`${root}/limitationLastProduct/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        value_limitation: newLimitation
      })
    })
    return await result.json()
  }
}
