import { shallowMount } from '@vue/test-utils'
import LastProducts from '@/components/Home/LastProducts.vue'
import { Carousel, Slide } from 'vue-carousel'
import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)

test('si se ejecuta el mounted ', async () => {
  API.loadLastProductsList = jest.fn()

  const mockCLastProducts = [
    {
      categoryName: 'All-Categories',
      idCategory: '1'
    },
    {
      categoryName: 'Categorie 1',
      idCategory: '2'
    },
    {
      categoryName: 'Categorie 2',
      idCategory: '3'
    },
    {
      categoryName: 'Categorie 3',
      idCategory: '4'
    }
  ]

  API.loadLastProductsList.mockReturnValue(mockCLastProducts)

  const wrapper = shallowMount(LastProducts, {
    data() {
      return {
        lastProductList: []
      }
    }
  })

  await finishAsyncTasks()
  expect(API.loadLastProductsList).toHaveBeenCalled()
  expect(wrapper.vm.lastProductList).toEqual(mockCLastProducts)
})
