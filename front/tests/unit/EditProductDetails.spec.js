import { shallowMount, createLocalVue } from '@vue/test-utils'
import EditProductDetails from '@/components/ProductInfo/EditProductDetails.vue'

test(' que se envia el evento save-new-details-event', async () => {
  const productY = {
    idSearched: '0000001',
    name: 'movilx2',
    description: 'aparato barato',
    price: 300,
    category: 'cat 2',
    tag1: 'tag 2',
    tag2: 'tag 2',
    tag3: 'tag 2',
    urlphoto: 'urlfoto'
  }
  const wrapper = shallowMount(EditProductDetails, {
    propsData: {
      product: productY,
      categories: ['cat1', 'cat2', 'cat3']
    },
    data() {
      return {
        localProduct: {}
      }
    }
  })
  const name = wrapper.find('.name-input')
  const description = wrapper.find('.description-input')
  const price = wrapper.find('.price-input')
  const category = wrapper.find('.category-input')
  const tag1 = wrapper.find('.tag1-input')
  const tag2 = wrapper.find('.tag2-input')
  const tag3 = wrapper.find('.tag3-input')

  name.setValue('movilx2')
  description.setValue('aparato barato')
  price.setValue(300)
  category.setValue('cat 2')
  tag1.setValue('tag 22')
  tag2.setValue('tag 22')
  tag3.setValue('tag 22')

  expect(wrapper.vm.localProduct).toStrictEqual({
    idSearched: '0000001',
    name: 'movilx2',
    description: 'aparato barato',
    price: '300',
    category: undefined,
    tag1: 'tag 22',
    tag2: 'tag 22',
    tag3: 'tag 22',
    urlphoto: 'urlfoto'
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const saveButton = wrapper.find('.SaveChangesButton')
  saveButton.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'save-new-details-event',
      args: [
        {
          idSearched: '0000001',
          name: 'movilx2',
          description: 'aparato barato',
          price: '300',
          category: undefined,
          tag1: 'tag 22',
          tag2: 'tag 22',
          tag3: 'tag 22',
          urlphoto: 'urlfoto'
        }
      ]
    }
  ])
})

test(' cancel-edit-product-details-event al clic en el button cancel', async () => {
  const productY = {
    idSearched: '0000001',
    name: 'movilx2',
    description: 'aparato barato',
    price: 300,
    category: 'cat 2',
    tag1: 'tag 2',
    tag2: 'tag 2',
    tag3: 'tag 2'
  }

  const wrapper = shallowMount(EditProductDetails, {
    propsData: {
      product: productY,
      categories: ['cat1', 'cat2', 'cat3']
    },
    data() {
      return {
        localProduct: {}
      }
    }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const reservButton = wrapper.find('.cancel-edit')
  reservButton.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'cancel-edit-product-details-event',
      args: []
    }
  ])
})
