import { shallowMount } from '@vue/test-utils'
import AddUserPage from '@/components/AddUser/AddUserPage.vue'
import API from '@/API.js'
const finishAsyncTasks = () => new Promise(setImmediate)

test('the input are saved ', async () => {
  API.saveNewUser = jest.fn()
  const wrapper = shallowMount(AddUserPage, {
    data() {
      return {
        confirmPassword: null,

        user: {
          name: null,
          hasImage: false,
          image: null,
          mail: null,
          password: null
        }
      }
    }
  })
  expect(wrapper.emitted().search).toBe(undefined)
  // Simular lo que hace el usuario.

  const input1 = wrapper.find('.nickname-input')
  const input2 = wrapper.find('.mail-input')
  const input3 = wrapper.find('.passeword-input')
  const input4 = wrapper.find('.Repasseword-input')
  const AÑADIR = wrapper.find('.adduser')

  input1.setValue('yeray')
  input2.setValue('yeray@penascal.com')
  input3.setValue('1234567890')
  input4.setValue('1234567890')

  expect(wrapper.vm.user.nickname).toBe('yeray')
  expect(wrapper.vm.user.mail).toBe('yeray@penascal.com')
  expect(wrapper.vm.user.password).toBe('1234567890')
  expect(wrapper.vm.confirmPassword).toBe('1234567890')
})

test('si funciona la function saveNewUser', async () => {
  API.saveNewUser = jest.fn()
  const mockUser = [
    {
      iduser: '00001',
      nickname: 'yeray',
      urlphoto: 'url foto',
      mail: 'mail@mail.com',
      password: 'password',
      datecreate: '01/01/2020'
    }
  ]
  API.saveNewUser.mockReturnValue(mockUser)

  const wrapper = shallowMount(AddUserPage, {
    data() {
      return {}
    }
  })

  const submit = wrapper.find('.adduser')
  submit.trigger('click')
  await finishAsyncTasks()
  expect(API.saveNewUser).toHaveBeenCalled()
})
