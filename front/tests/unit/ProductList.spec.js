import { shallowMount } from '@vue/test-utils'
import ProductList from '@/components/ProductList/ProductList.vue'
import ProductBox from '@/components/ProductList/ProductBox.vue'
import API from '@/API.js'

test('estado initial', () => {
  API.loadProductList = jest.fn()
  API.loadCategories = jest.fn()

  const wrapper = shallowMount(ProductList, {
    mocks: {
      $route: {
        params: { searchedcategoryrutes: 'cat 1' }
      }
    },
    data() {
      return {
        productList: []
      }
    }
  })
  const productBox = wrapper.findAll(ProductBox).wrappers

  expect(productBox.length).toBe(0)
})

test('que en Productlist se pinta todo lo contenido de productlist[]', () => {
  API.loadProductList = jest.fn()
  API.loadCategories = jest.fn()
  const wrapper = shallowMount(ProductList, {
    mocks: {
      $route: {
        params: { searchedcategoryrutes: 'cat 1' }
      }
    },
    data() {
      return {
        productList: [
          {
            id_product: '0000001',
            name: 'movilx1',
            price: 400
          },
          {
            id_product: '0000002',
            name: 'movilx2',
            price: 300
          }
        ]
      }
    }
  })
  const productBox = wrapper.findAll(ProductBox).wrappers

  expect(productBox.length).toEqual(2)
})

const finishAsyncTasks = () => new Promise(setImmediate)

test('al montar el component se ejecuta loadProductList() si params es undefined ', async () => {
  API.loadProductList = jest.fn()
  API.loadCategories = jest.fn()
  const mockProductList = [
    {
      id_product: '0000001',
      name: 'movilx1',
      price: 400
    },
    {
      id_product: '0000002',
      name: 'movilx2',
      price: 300
    }
  ]
  API.loadProductList.mockReturnValue(mockProductList)
  const wrapper = shallowMount(ProductList, {
    mocks: {
      $route: {
        params: [
          { searchedcategoryrutes: undefined },
          { searchedProductsNameroute: undefined }
        ]
      }
    },
    data() {
      return {
        productList: []
      }
    }
  })
  // await wrapper.vm.$nextTick()
  await finishAsyncTasks()
  expect(API.loadProductList).toHaveBeenCalled()
  expect(wrapper.vm.productList).toEqual(mockProductList)
})

test('al montar el component se ejecuta this.searchForProductsPerCategory() si params es defined ', async () => {
  API.loadProductList = jest.fn()
  API.loadCategories = jest.fn()
  API.loadProductsDetailsBySearchedCategory = jest.fn()
  const mockProductList = [
    {
      id_product: '0000001',
      name: 'movilx1',
      price: 400,
      category: 'cat 2'
    },
    {
      id_product: '0000002',
      name: 'movilx2',
      price: 300,
      category: 'cat 2'
    }
  ]
  API.loadProductsDetailsBySearchedCategory.mockReturnValue(mockProductList)
  const wrapper = shallowMount(ProductList, {
    mocks: {
      $route: {
        params: [
          { searchedcategoryrutes: 'cat 2' },
          { searchedProductsNameroute: undefined }
        ]
      }
    },
    data() {
      return {
        searchedcategoryrutes: 'cat 2',
        productList: [],
        searchedCategory: 'cat 2'
      }
    }
  })
  await finishAsyncTasks()
  expect(API.loadProductsDetailsBySearchedCategory).toHaveBeenCalled()
  expect(wrapper.vm.productList).toEqual(mockProductList)
  expect(wrapper.vm.searchedcategoryrutes).toBe(null)
})
