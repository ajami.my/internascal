import { shallowMount, createLocalVue } from '@vue/test-utils'
import ShowProductDetails from '@/components/ProductInfo/ShowProductDetails.vue'

test('Comprobamos que información de App imprimimos en la pantalla', () => {
  //crear componente con las propiedades
  //comprobar que el nombre y el apellido estan en su sitio
  const wrapper = shallowMount(ShowProductDetails, {
    propsData: {
      product: {
        name: 'movilx2',
        description: 'aparato barato',
        price: 300,
        category: 'cat 2',
        tag1: 'tag 2',
        tag2: 'tag 2',
        tag3: 'tag 2'
      }
    }
  })
  //buscamos la clase de la plantilla html (find)
  const name = wrapper.find('.name-output')
  const description = wrapper.find('.description-output')
  const price = wrapper.find('.price-output')
  const category = wrapper.find('.category-output')
  const tag1 = wrapper.find('.tag1-output')
  const tag2 = wrapper.find('.tag2-output')
  const tag3 = wrapper.find('.tag3-output')

  expect(name.text()).toBe('movilx2')
  expect(description.text()).toBe('aparato barato')
  expect(price.text()).toBe('300')
  expect(category.text()).toBe('cat 2')
  expect(tag1.text()).toBe('tag 2')
  expect(tag2.text()).toBe('tag 2')
  expect(tag3.text()).toBe('tag 2')
})

test(' que se envia el evento reserve-product-event', async () => {
  const productY = {
    idSearched: '0000001',
    name: 'movilx2',
    description: 'aparato barato',
    price: 300,
    category: 'cat 2',
    tag1: 'tag 2',
    tag2: 'tag 2',
    tag3: 'tag 2'
  }
  const wrapper = shallowMount(ShowProductDetails, {
    propsData: { product: productY },
    data() {
      return {
        searchedUserId: null
      }
    }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const reservButton = wrapper.find('.reserveProduct')
  reservButton.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'reserve-product-event',
      args: ['0000001']
    }
  ])
})

test(' que se envia el evento edit-product-details-event al click edit button', async () => {
  const productY = {
    idSearched: '0000001',
    name: 'movilx2',
    description: 'aparato barato',
    price: 300,
    category: 'cat 2',
    tag1: 'tag 2',
    tag2: 'tag 2',
    tag3: 'tag 2'
  }

  const wrapper = shallowMount(ShowProductDetails, {
    propsData: { product: productY },
    data() {
      return {
        searchedUserId: null
      }
    }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const reservButton = wrapper.find('.edit-product')
  reservButton.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'edit-product-details-event',
      args: []
    }
  ])
})
