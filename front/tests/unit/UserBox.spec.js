import { shallowMount } from '@vue/test-utils'
import UserBox from '@/components/UserList/UserBox.vue'

test('pinta en la pantalla', () => {
  const wrapper = shallowMount(UserBox, {
    propsData: {
      userBoxDetails: {
        idActualUser: '0000001',
        nickname: 'yassine',
        email: 'yassine@gmail.com',
        role: 'moderador',
        penascales: 400
      }
    }
  })

  expect(wrapper.text()).toContain('yassine')
  expect(wrapper.text()).toContain('yassine@gmail.com')
  expect(wrapper.text()).toContain('moderador')
  expect(wrapper.text()).toContain('400')
})

test(' que se envia el evento go-to-this-product-details', async () => {
  const user = {
    iduser: '0000001',
    nickname: 'yassine',
    email: 'yassine@gmail.com',
    role: 'moderador',
    penascales: 400
  }
  const wrapper = shallowMount(UserBox, {
    propsData: { userBoxDetails: user },
    data() {
      return {
        searchedUserId: null
      }
    }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const seeMoreDetailsOnClick = wrapper.find('.addUser')
  seeMoreDetailsOnClick.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['go-to-this-user-details'].length).toBe(1)
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'go-to-this-user-details',
      args: ['0000001']
    }
  ])
})

// test("Crear un Computed para fullName", async () => {
//     const wrapper = shallowMount(ProductBox, {
//         propsData: {
//             details: {
//                 id: "01",
//                 name: "yassin",
//                 familyName: "ajami"
//             }
//         }
//     });
//     await wrapper.vm.$nextTick();
//     expect(wrapper.vm.fullName).toContain("yassin ajami");
// });
