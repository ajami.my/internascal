import { shallowMount } from '@vue/test-utils'
import ProductBox from '@/components/ProductList/ProductBox.vue'

test('pinta en la pantalla', () => {
  const wrapper = shallowMount(ProductBox, {
    propsData: {
      productBoxDetails: {
        idproduct: '0000001',
        name: 'movilx1',
        price: 400
      }
    }
  })

  expect(wrapper.text()).toContain('movilx1')
  expect(wrapper.text()).toContain('400')
})

test(' que se envia el evento go-to-this-product-details', async () => {
  const product = {
    idproduct: '0000001',
    name: 'movilx1',
    price: 400
  }
  const wrapper = shallowMount(ProductBox, {
    propsData: { productBoxDetails: product }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const seeMoreDetailsOnClick = wrapper.find('.product-box')
  seeMoreDetailsOnClick.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['go-to-this-product-details'].length).toBe(1)
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'go-to-this-product-details',
      args: ['0000001']
    }
  ])
})

// test("Crear un Computed para fullName", async () => {
//     const wrapper = shallowMount(ProductBox, {
//         propsData: {
//             details: {
//                 id: "01",
//                 name: "yassin",
//                 familyName: "ajami"
//             }
//         }
//     });
//     await wrapper.vm.$nextTick();
//     expect(wrapper.vm.fullName).toContain("yassin ajami");
// });
