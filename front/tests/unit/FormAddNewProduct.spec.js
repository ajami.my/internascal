import { shallowMount } from '@vue/test-utils'
import FormAddNewProduct from '@/components/AddProduct/FormAddNewProduct.vue'
import ImageUploader from 'vue-image-upload-resize'

test('emite event Upload new product ', async () => {
  const wrapper = shallowMount(FormAddNewProduct, {
    propsData: {
      categories: {
        propType: Array,
        required: true
      }
    },
    data() {
      return {
        product: {
          name: null,
          hasImage: false,
          image: null,
          price: null,
          category: null,
          description: null,
          tag1: null,
          tag2: null,
          tag3: null,
          date_create: null
        }
      }
    }
  })

  expect(wrapper.emitted().search).toBe(undefined)

  const inputTag1 = wrapper.find('.tag1')
  const inputTag2 = wrapper.find('.tag2')
  const inputTag3 = wrapper.find('.tag3')
  const inputName = wrapper.find('.name-input')
  const inputPrice = wrapper.find('.price-input')

  const textAreaDescription = wrapper.find('.description')
  const submit = wrapper.find('.addProduct')

  inputTag1.setValue('tag1')
  inputTag2.setValue('tag2')
  inputTag3.setValue('tag3')
  inputName.setValue('movil')
  inputPrice.setValue(100)

  textAreaDescription.setValue(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
  )

  submit.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['upload-new-product'].length).toBe(1)
  expect(wrapper.emitted()['upload-new-product']).toEqual([
    [
      {
        name: 'movil',
        price: '100',
        tag1: 'tag1',
        tag2: 'tag2',
        tag3: 'tag3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        hasImage: false,
        image: null,
        date_create: null,
        category: null
      }
    ]
  ])
})
