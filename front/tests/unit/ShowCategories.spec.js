import { shallowMount } from '@vue/test-utils'
import ShowCategories from '@/components/ShowCategories/ShowCategories.vue'
import ShowCategoryBox from '@/components/ShowCategories/ShowCategoryBox.vue'
import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)

test('pinta lista vacío', () => {
  API.loadCategories = jest.fn()
  const wrapper = shallowMount(ShowCategories, {
    propsData: {
      categories: []
    }
  })
  const showCategoryCard = wrapper.findAll(ShowCategoryBox).wrappers

  expect(showCategoryCard.length).toBe(0)
})

test('si funciona la function loadCategories ', async () => {
  API.loadCategories = jest.fn()
  const mockShowCategories = [
    {
      id: '01',
      categoryName: 'cat1'
    },
    {
      id: '02',
      categoryName: 'cat2'
    }
  ]
  API.loadCategories.mockReturnValue(mockShowCategories)

  const wrapper = shallowMount(ShowCategories, {
    data() {
      return {
        categories: []
      }
    }
  })

  await finishAsyncTasks()

  expect(API.loadCategories).toHaveBeenCalled()

  expect(wrapper.vm.categories).toEqual(mockShowCategories)
})
