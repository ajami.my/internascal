import { shallowMount } from '@vue/test-utils'
import ShowCategoryBox from '@/components/ShowCategories/ShowCategoryBox.vue'

test('pinta en la pantalla', () => {
  const wrapper = shallowMount(ShowCategoryBox, {
    propsData: {
      category: {
        idCategory: '01',
        categoryName: 'cat 1'
      }
    }
  })

  expect(wrapper.text()).toContain('cat 1')
})

test('emite event goToCategory click', async () => {
  const category = {
    id: '01'
  }
  const wrapper = shallowMount(ShowCategoryBox, {
    propsData: {
      category: {
        idCategory: '01',
        categoryName: 'cat 1'
      }
    },
    data() {
      return {
        searchedCategoryName: 'cat 1'
      }
    }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const goToCategoryDiv = wrapper.find('.show-category-box')
  goToCategoryDiv.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['go-to-category'].length).toBe(1)
  expect(wrapper.emitted()['go-to-category'][0]).toEqual(['cat 1'])
})
