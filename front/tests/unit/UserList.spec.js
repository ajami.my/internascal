import { shallowMount } from '@vue/test-utils'
import UserList from '@/components/UserList/UserList.vue'
import UserBox from '@/components/UserList/UserBox.vue'
import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)

test('la function loadUserList  ', async () => {
  API.loadUserList = jest.fn()
  const mockResultUsers = [
    {
      iduser: '01',
      nickname: 'yass'
    },
    {
      iduser: '02',
      nickname: 'louis'
    }
  ]
  API.loadUserList.mockReturnValue(mockResultUsers)

  const wrapper = shallowMount(UserList, {
    data() {
      return {
        userList: []
      }
    }
  })

  await finishAsyncTasks()

  expect(API.loadUserList).toHaveBeenCalled()

  expect(wrapper.vm.userList).toEqual(mockResultUsers)
})

test('que en Productlist se pinta todo lo contenido de productlist[]', () => {
  API.loadUserList = jest.fn()
  const wrapper = shallowMount(UserList, {
    data() {
      return {
        userList: [
          {
            nickname: 'yassine',
            email: 'yassine@gmail.com',
            role: 'moderador'
          },
          {
            nickname: 'unai',
            email: 'unai@gmail.com',
            role: 'admin'
          }
        ]
      }
    }
  })
  const userBox = wrapper.findAll(UserBox).wrappers

  expect(userBox.length).toEqual(2)
})
