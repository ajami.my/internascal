import { shallowMount } from '@vue/test-utils'
import FormProductPage from '@/components/AddProduct/FormProductPage.vue'
import FormAddNewProduct from '@/components/AddProduct/FormAddNewProduct.vue'
import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)

test('si funciona la function loadCategories de component add product ', async () => {
  API.loadCategories = jest.fn()
  const mockResultCategories = [
    {
      id: '01',
      categoryName: 'cat1'
    },
    {
      id: '02',
      categoryName: 'cat2'
    }
  ]
  API.loadCategories.mockReturnValue(mockResultCategories)

  const wrapper = shallowMount(FormProductPage, {
    data() {
      return {
        categories: []
      }
    }
  })

  await finishAsyncTasks()

  expect(API.loadCategories).toHaveBeenCalled()

  expect(wrapper.vm.categories).toEqual(mockResultCategories)
})

test('si funciona la function saveNewProduct', async () => {
  API.saveNewProduct = jest.fn()
  const mockNewProduct = [
    {
      idproduct: 'uuidv4()',
      name: 'product.name',
      description: 'product.description',
      category: 'product.category',
      price: 'product.price',
      urlphoto: 'product.image.dataUrl',
      tag1: 'product.tag1',
      tag2: 'product.tag2',
      tag3: 'product.tag3',
      datecreate: 'product.date_create'
    }
  ]
  API.saveNewProduct.mockResolvedValue(mockNewProduct)

  const wrapper = shallowMount(FormProductPage, {
    data() {
      return {}
    }
  })

  await finishAsyncTasks()
  const formAddNewProduct = wrapper.find(FormAddNewProduct)
  formAddNewProduct.vm.$emit('upload-new-product', mockNewProduct)

  expect(API.saveNewProduct).toHaveBeenCalled()
})
