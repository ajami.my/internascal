import { shallowMount, createLocalVue } from '@vue/test-utils'
import ProductInfoPage from '@/components/ProductInfo/ProductInfoPage.vue'
import ShowProductDetails from '@/components/ProductInfo/ShowProductDetails.vue'
import EditProductDetails from '@/components/ProductInfo/EditProductDetails.vue'

import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)

test('si se ejecuta el mounted ', async () => {
  API.loadProductDetails = jest.fn()
  API.loadCategories = jest.fn()
  const mockProduct = [
    {
      idproduct: '0000000',
      name: 'movil',
      urlphoto: null,
      price: 300,
      category: 'high-tech',
      description: 'Lorem ipsum dolor sit amet ',
      tag1: '4G',
      tag2: 'nokia',
      tag3: 'lumina'
    }
  ]
  const mockCategories = [
    {
      categoryName: 'All-Categories',
      idCategory: '1'
    },
    {
      categoryName: 'Categorie 1',
      idCategory: '2'
    },
    {
      categoryName: 'Categorie 2',
      idCategory: '3'
    },
    {
      categoryName: 'Categorie 3',
      idCategory: '4'
    }
  ]
  API.loadCategories.mockResolvedValue(mockCategories)
  API.loadProductDetails.mockResolvedValue(mockProduct)

  const wrapper = shallowMount(ProductInfoPage, {
    mocks: {
      $route: {
        params: { id_product: '0000000' }
      }
    },
    data() {
      return {
        idSearched: null,
        categories: null,
        role: 'admin',
        ifEditProduct: false,
        product: {},
        urlLoadCategories: '/api/categories',
        categories: null
      }
    }
  })

  await finishAsyncTasks()

  expect(API.loadProductDetails).toHaveBeenCalled()
  expect(API.loadCategories).toHaveBeenCalled()

  //expect(wrapper.vm.categories).toEqual(mockCategories)

  expect(wrapper.vm.product).toEqual(mockProduct)
})

test("cuando recibe 'edit-product-details-event', cambia la pantalla ", async () => {
  API.loadProductDetails = jest.fn()
  API.loadCategories = jest.fn()
  const wrapper = shallowMount(ProductInfoPage, {
    mocks: {
      $route: {
        params: { id_product: '0000000' }
      }
    },
    data() {
      return {
        idSearched: '0000000',
        hasShow: true,
        hasEdit: false
      }
    }
  })

  const showProductDetails = wrapper.find(ShowProductDetails)

  showProductDetails.vm.$emit('edit-product-details-event')
  expect(wrapper.vm.hasShow).toBe(false)
  expect(wrapper.vm.hasEdit).toBe(true)
})

test("cuando recibe 'save-new-details-event', ejecuta la funcion async saveNewProduct ", async () => {
  API.loadProductDetails = jest.fn()
  API.loadCategories = jest.fn()
  API.saveNewProduct = jest.fn()
  const wrapper = shallowMount(ProductInfoPage, {
    mocks: {
      $route: {
        params: { id_product: '0000000' }
      }
    },
    data() {
      return {
        idSearched: '0000000',
        hasShow: true,
        hasEdit: false
      }
    }
  })

  const showProductDetails = wrapper.find(ShowProductDetails)

  showProductDetails.vm.$emit('save-new-details-event')
  await finishAsyncTasks()
  expect(API.loadProductDetails).toHaveBeenCalled()
  expect(wrapper.vm.hasEdit).toBe(false)
})

// test('comprobar el watch ', async () => {
//     API.loadProductDetails = jest.fn()
//     API.loadCategories = jest.fn()

//     const wrapper = shallowMount(ProductInfoPage, {
//         mocks: {
//             $route: {
//                 params: { id_product: '0000000' },
//             },
//         },

//         data() {
//             return {
//                 role: 'admin',
//                 product: {
//                     id_product: '0000000',
//                     name: 'movil',
//                     hasImage: false,
//                     image: null,
//                     price: 300,
//                     category: 'high-tech',
//                     description: 'Lorem ipsum dolor sit amet ',
//                     tag1: '4G',
//                     tag2: 'nokia',
//                     tag3: 'lumina',
//                     edit: false,
//                 },
//             }

//         },
//     })
//     expect(wrapper.vm.localproduct).toEqual(wrapper.vm.product)
// })
