import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserInfoPage from '@/components/UserInfo/UserInfoPage.vue'
import ImageUploader from 'vue-image-upload-resize'
import API from '@/API.js'

const finishAsyncTasks = () => new Promise(setImmediate)
test('Comprobamos que los detailles que llegan se impriman en la pantalla ', () => {
  API.loadUserDetails = jest.fn()
  API.loadUserRoles = jest.fn()

  const wrapper = shallowMount(UserInfoPage, {
    mocks: {
      $route: {
        params: { idActualUser: '01' }
      }
    },

    data() {
      return {
        idSearchedUser: this.$route.params.id_product,
        roles: [],
        role: 'admin',
        ifEditUser: false,
        product: {
          iduser: '01',
          nickname: 'mikel',
          mail: 'mikel@peñascal.com',
          role: '',
          penascales: '99',
          urlphoto: null
        }
      }
    }
  })

  expect(wrapper.text()).toContain('user.nickname :')
  expect(wrapper.text()).toContain('user.penascales :')
  expect(wrapper.text()).toContain('user.role -:')
})

test('Funciona el Mounted y carga datos del json con la funcion  PutUserEnData', async () => {
  API.loadUserDetails = jest.fn()
  API.loadUserRoles = jest.fn()
  const mockUser = [
    {
      iduser: '01',
      nickname: 'yass',
      role: 'admin',
      penascales: '900',
      urlphoto: null
    }
  ]
  API.loadUserDetails.mockReturnValue(mockUser)
  const wrapper = shallowMount(UserInfoPage, {
    mocks: {
      $route: {
        params: { idActualUser: '01' }
      }
    },
    data() {
      return {
        user: {}
      }
    }
  })
  await finishAsyncTasks()
  expect(API.loadUserDetails).toHaveBeenCalled()
  expect(wrapper.vm.user).toEqual(mockUser)
})

test('si funciona la function loadRoles ', async () => {
  API.loadUserDetails = jest.fn()
  API.loadUserRoles = jest.fn()
  const mockRoles = [
    {
      id: '01',
      role: 'role1'
    },
    {
      id: '02',
      role: 'role2'
    }
  ]
  API.loadUserRoles.mockReturnValue(mockRoles)

  const wrapper = shallowMount(UserInfoPage, {
    mocks: {
      $route: {
        params: { idActualUser: '01' }
      }
    },
    data() {
      return {
        roles: []
      }
    }
  })

  await finishAsyncTasks()

  expect(API.loadUserRoles).toHaveBeenCalled()

  expect(wrapper.vm.roles).toEqual(mockRoles)
})
