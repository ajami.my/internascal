<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class Products extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE products (
	            idproduct	TEXT NOT NULL,
	            name	TEXT,
	            description	TEXT,
	            category	TEXT NOT NULL,
	            price	INTEGER NOT NULL,
	            urlphoto	TEXT,
	            tag1	TEXT,
	            tag2	TEXT,
                tag3	TEXT,
                datecreate TEXT NOT NULL,
	            PRIMARY KEY(idproduct)
           );
            INSERT INTO products VALUES ('1','name1', 'descr 1', 'cat 1', '333','url1', 'tag1', 'tag2', 'tag3', '2020-03-01');
            INSERT INTO products VALUES ('2','name2', 'descr 2', 'cat 2', '222','url2', 'tag1', 'tag2', 'tag3', '2020-03-02');
        ");
       
    }
    public function testGetProductlist()
    {
        $this->json('GET', 'api/products')
            ->assertStatus(200)
           
            ->assertJson([
                [
                    'idproduct' => '1',
                    'name' => 'name1',
                    'price' => '333',
                    'urlphoto' => 'url1',
                ],
                [
                    'idproduct' => '2',
                    'name' => 'name2',
                    'price' => '222',
                    'urlphoto' => 'url2',
                ],
            ]);
    }
    public function testGetProduct()
    {
        $this->json('GET', 'api/products/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'idproduct' => '1',
                    'name' => 'name1',
                    'description' => 'descr 1',
                    'category' => 'cat 1',
                    'price' => '333',
                    'urlphoto' => 'url1',
                    'tag1' => 'tag1',
                    'tag2' => 'tag2',
                    'tag3' => 'tag3',
                    'datecreate' => '2020-03-01',

                ]
            );
    }
    public function testPut()
    {
        $data = [
            'idproduct' => '1',
            'name' => 'name1 modified',
            'description' => 'descr 1',
            'category' => 'cat 1',
            'price' => '333',
            'urlphoto' => 'url1',
            'tag1' => 'tag1 modified',
            'tag2' => 'tag2',
            'tag3' => 'tag3',
           

        ];
        $expected = [
            'idproduct' => '1',
            'name' => 'name1 modified',
            'description' => 'descr 1',
            'category' => 'cat 1',
            'price' => '333',
            'urlphoto' => 'url1',
            'tag1' => 'tag1 modified',
            'tag2' => 'tag2',
            'tag3' => 'tag3',
           
        ];
        $this->json('PUT', 'api/products/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);
        $this->json('GET', 'api/products/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }
    public function testPostProduct()
    {
        $this->json('POST', '/api/products', [
            'idproduct' => '99',
            'name' => 'name3',
            'description' => 'descr 3',
            'category' => 'cat 3',
            'price' => '666',
            'urlphoto' => 'url3',
            'tag1' => 'tag1',
            'tag2' => 'tag2',
            'tag3' => 'tag3',
            'datecreate' => '2020-03-01',
        ])->assertStatus(200);
        $this->json('GET', 'api/products/99')
            ->assertStatus(200)
            ->assertJson([
                'idproduct' => '99',
                'name' => 'name3',
                'description' => 'descr 3',
                'category' => 'cat 3',
                'price' => '666',
                'urlphoto' => 'url3',
                'tag1' => 'tag1',
                'tag2' => 'tag2',
                'tag3' => 'tag3',
                'datecreate' => '2020-03-01',
            ]);
    }
}

//   ------------------------------- TEST TABLE USERS ---------------------------------        

class Users extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE users (
	            iduser	TEXT NOT NULL,
                nickname	TEXT NOT NULL,
                mail    TEXT NOT NULL,
	            password	TEXT NOT NULL,
	            role	TEXT NOT NULL DEFAULT 'normal',
                penascales	NUMERIC NOT NULL DEFAULT 0,
                urlphoto    TEXT ,
                datecreate TEXT NOT NULL,
	            PRIMARY KEY(iduser)
           );
            INSERT INTO users VALUES ('1','nickname1', 'mail 1', 'password 1', 'admin','111', 'url1', '2020-04-02');
            INSERT INTO users VALUES ('2','nickname2', 'mail 2', 'password 2', 'normal','222', 'url2', '2020-04-03');
        ");
        
    }
    public function testGetUserlist()
    {
        $this->json('GET', 'api/users')
            ->assertStatus(200)
            ->assertJson([
                [
                    'iduser' => '1',
                    'nickname' => 'nickname1',
                    'mail' => 'mail 1',
                   
                    'role' => 'admin',
                    'penascales' => '111',
                    'urlphoto' => 'url1',
                ],
                [
                    'iduser' => '2',
                    'nickname' => 'nickname2',
                    'mail' => 'mail 2',
                   
                    'role' => 'normal',
                    'penascales' => '222',
                    'urlphoto' => 'url2',
                ],
            ]);
    }

    public function testGetUser()
    {
        $this->json('GET', 'api/users/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'iduser' => '1',
                    'nickname' => 'nickname1',
                    'mail' => 'mail 1',
                   
                    'role' => 'admin',
                    'penascales' => '111',
                    'urlphoto' => 'url1',
                   
                ]

            );
    }



    public function testPutUser()
    {
        $data = [
            'iduser' => '1',
            'nickname' => 'nickname1 modified',
            'role' => 'admin',
            'penascales' => '333',
            'urlphoto' => 'url1 modified',
            'datecreate' => '2020-04-02',
        ];
        $expected = [
            'iduser' => '1',
            'nickname' => 'nickname1 modified',
            'mail' => 'mail 1',
            'role' => 'admin',
            'penascales' => '333',
            'urlphoto' => 'url1 modified',
         
        ];
        $this->json('PUT', 'api/users/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);
        $this->json('GET', 'api/users/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }



    public function testPostUser()
    {
        $this->json('POST', '/api/users', [
            'iduser' => '99',
            'nickname' => 'nickname3',
            'mail' => 'mail 3',
            'password' => 'password 3',
            'datecreate' => '2020-04-05', 
            'datecreate' => '2020-04-05',
        ])->assertStatus(200);
        $this->json('GET', 'api/users/99')
            ->assertStatus(200)
            ->assertJson([
            'iduser' => '99',
            'nickname' => 'nickname3',
            'mail' => 'mail 3',
            'role' => 'normal',
            'penascales' => '0',
            'urlphoto' => null,
                
            ]);
    }
}
