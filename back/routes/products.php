<?php

Route::get('/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto, new_price, promos from products where state="Disponible"');
    return response()->json($results, 200);
});

Route::get('/products/promos', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto, new_price from products where state="Disponible" and promos=1 ');
    return response()->json($results, 200);
});

Route::get('/products/more-viewed-categories/', function (Request $request) {
    $results = DB::select('select category, sum(views) as viewCategory
    from products
    group by category
    order by category');
    return response()->json($results, 200);
});

Route::get('/products/load-products-en-await', function (Request $request) {
    $results = DB::select('select P.idproduct, P.name, p.price, p.promos, p.urlphoto, U.nickname, U.iduser
    from products p
    left join users U on U.iduser=P.id_prop
    where P.state="En Espera" ');
    return response()->json($results, 200);
});

Route::get('/products/load-rejected-products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto from products where state="Rechazado" ');
    return response()->json($results, 200);
});
Route::get('/products/load-confirmed-products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto, promos  from products where state="Confirmado" ');
    return response()->json($results, 200);
});

Route::get('/products/load-reserved-products', function (Request $request) {
    $results = DB::select('select R.idReservation,R.idproduct,R.iduser,U.iduser, U.nickname, P.name, P.price,P.urlphoto, p.state, p.promos
    from reservations R
    left join users U on U.iduser=R.iduser
    left join products P on P.idproduct=R.idproduct
    where R.stat="Reservation" ');
    return response()->json($results, 200);
});

Route::get('/last-products/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto from products where state="Disponible"
     ORDER BY datecreate desc LIMIT 0,(select value_limitation from limitation_last_product)');
    return response()->json($results, 200);
});

Route::get('/more-viewed/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto, views from products where state="Disponible" order by views desc LIMIT 0,(select value_limitation from limitation_last_product) ');
    return response()->json($results, 200);
});

// Route::get('/featured/products', function (Request $request) {
//     $results = DB::select('select idproduct, name, price, urlphoto from products where featured="true" ORDER BY datecreate desc');
//     return response()->json($results, 200);
// });

Route::get('/promos/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto from products where promos=1 and state="Disponible" ORDER BY datecreate desc');
    return response()->json($results, 200);
});

Route::get('/search_product/products/{txt}', function ($txt) {

    $results = DB::select('select name from products WHERE state="Disponible" and promos=0 and name LIKE "%' . $txt . '%" and state="Disponible"');
    return response()->json($results, 200);
});

Route::get('/search_product_by_name/products/{name}', function ($name) {

    $results = DB::select('select idproduct, name, price, urlphoto, promos from products where name LIKE "%' . $name . '%" and state="Disponible" '

    )
    ;
    return response()->json($results, 200);
});

Route::get('/products/categories/{category}', function ($category) {

    if (categoryNoExists($category)) {
        $results = [
            'categoria' => 'false',
        ];

        return response()->json($results, 404);
    }

    $results = DB::select('select * from products where category=:category and state="Disponible"',
        [
            'category' => $category,
        ]);
    return response()->json($results, 200);
});

Route::get('/products/{idproduct}', function ($idproduct) {

    if (productNoExists($idproduct)) {
        abort(404);
    }
    $results = DB::select('select * from products where idproduct=:idproduct ', [
        'idproduct' => $idproduct,
    ]);

    DB::update("update products set views=views+1 where idproduct=:idproduct",
        [
            'idproduct' => $idproduct,
        ]);

    return response()->json($results[0], 200);
});

Route::get('/my_products/{id_user}', function ($id_user) {

    $results = DB::select('select idproduct, name, price, urlphoto, state from products
     where id_prop=:id_user   ',
        [
            'id_user' => $id_user,
        ]);
    return response()->json($results, 200);
});




Route::post('/products', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into products (idproduct, name, description, category, price, urlphoto, id_prop, datecreate, state)
        VALUES (:idproduct, :name, :description, :category, :price, :urlphoto, :id_prop, :datecreate,case
        (select role from users where iduser=:id_prop) when 'admin' then 'Disponible' 
        when 'vendedor' then 'Confirmado' else 'En Espera' end)
    ",
        $data
    );

    $results = DB::select('select * from products where idproduct = :idproduct', [
        'idproduct' => $data['idproduct'],
    ]);
    return response()->json($results[0], 200);
});









Route::PUT('/products/{idproduct}', function (Request $request, $idproduct) {
    $data = request()->all();
    if (productNoExists($idproduct)) {
        abort(404);
    }

    DB::update("update products set  name=:name, description=:description,  category=:category,  price=:price ,  urlphoto=:urlphoto,  tag1=:tag1,  tag2=:tag2,  tag3=:tag3, new_price=:new_price, promos=:promos  where idproduct=:idproduct",
        [
            'name' => $data['name'],
            'description' => $data['description'],
            'category' => $data['category'],
            'price' => $data['price'],
            'urlphoto' => $data['urlphoto'],
            'tag1' => $data['tag1'],
            'tag2' => $data['tag2'],
            'tag3' => $data['tag3'],
            'new_price' => $data['new_price'],
            'promos' => $data['promos'],
            'idproduct' => $data['idproduct'],

        ]);

    $results = DB::select('select idproduct, name, description, category, price, urlphoto, tag1, tag2, tag3 from products
 where idproduct=:idproduct', ['idproduct' => $idproduct]);
    return response()->json($results[0], 200);
});

Route::put('/updateState/products/{idproduct}', function ($idproduct) {

    $data = request()->all();
    DB::update("update products set state = 'Reservado'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );

    $results = [
        'succesful' => 'state updated',
    ];

    return response()->json($results, 200);
});

Route::put('/updateState/confirm/products/{idproduct}', function ($idproduct) {

    $data = request()->all();
    DB::update("update products set state = 'Confirmado'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );

    $results = [
        'succesful' => 'state updated',
    ];

    return response()->json($results, 200);
});

Route::put('/updateState/reject/products/{idproduct}', function ($idproduct) {

    $data = request()->all();
    DB::update("update products set state = 'Rechazado'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );
    $results = [
        'succesful' => 'state updated',
    ];

    return response()->json($results, 200);
});

Route::put('/updateState/confirmed-to-stock/products/{idproduct}', function ($idproduct) {

    $data = request()->all();
    DB::update("update products set state = 'Disponible'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );
    DB::update('update users set penascales=penascales+ ( select price from products where idproduct=:idproduct)
     where iduser=(select id_prop  from products where idproduct=:idproduct)',
        [

            'idproduct' => $idproduct,
        ]);

    $results = [
        'succesful' => 'state updated',
    ];
    return response()->json($results, 200);
});



Route::put('/updateState/to-stock/products/{idproduct}&{idReservation}&{id_user}', function ($idproduct, $idReservation, $id_user) {

    $data = request()->all();
    DB::update("update products set state = 'Disponible'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );
    DB::update('update users set penascales=penascales+ ( select price from products where idproduct=:idproduct)
     where iduser=(select iduser from reservations where idproduct=:idproduct and stat="Reservation")',
        [

            'idproduct' => $idproduct,
        ]);

    DB::update('update reservations set stat="Cancel", admin_confirmation=:id_user   where idReservation=:idReservation',
        [
            'idReservation' => $idReservation,
            'id_user' => $id_user,
        ]);
    $results = [
        'succesful' => 'state updated',
    ];
    return response()->json($results, 200);
});

Route::put('/updatePromoState/to-stock/products/{idproduct}&{idReservation}&{id_user}', function ($idproduct, $idReservation, $id_user) {

    $data = request()->all();
    DB::update("update products set state = 'Disponible'  where idproduct= :idproduct",
        [
            'idproduct' => $idproduct,
        ]
    );
    DB::update('update users set penascales=penascales+ ( select new_price from products where idproduct=:idproduct)
     where iduser=(select iduser from reservations where idproduct=:idproduct and stat="Reservation")',
        [

            'idproduct' => $idproduct,
        ]);

    DB::update('update reservations set stat="Cancel", admin_confirmation=:id_user   where idReservation=:idReservation',
        [
            'idReservation' => $idReservation,
            'id_user' => $id_user,
        ]);
    $results = [
        'succesful' => 'state updated',
    ];
    return response()->json($results, 200);
});

Route::put('/updateState/to-selled/products/{idproduct}&{idReservation}&{id_user}', function (Request $request, $idproduct, $idReservation, $id_user) {

    $data = request()->all();
    DB::update("update products set state = 'Comprado'  where idproduct= :idproduct",
        [
            $request->idproduct,
        ]
    );
    DB::update('update reservations set stat="Vendido", admin_confirmation=:id_user   where idReservation=:idReservation and stat="Reservation"',
        [
            'idReservation' => $idReservation,
            'id_user' => $id_user,
        ]);
    $results = [
        'succesful' => 'state updated',
    ];
    return response()->json($results, 200);
});
if (!function_exists('categoryNoExists')) {
    function categoryNoExists($category)
    {
        $results = DB::select('select * from products where category=:category', [
            'category' => $category,
        ]);
        return count($results) == 0;
    }
}
;
if (!function_exists('productNoExists')) {
    function productNoExists($idproduct)
    {
        $results = DB::select('select * from products where idproduct=:idproduct', [
            'idproduct' => $idproduct,
        ]);
        return count($results) == 0;
    }
}
