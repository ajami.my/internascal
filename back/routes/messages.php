<?php

Route::get('/get-messages/{id_user}', function ($id_user) {
    $data = request()->all();
    $results = DB::select('select * from messages where id_from = :id_user or id_to = :id_user ',
        [
            'id_user' => $id_user,
            'id_user' => $id_user,
        ]
    );
    return response()->json($results, 200);
});

Route::post('/add-messages', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into messages (id_message, id_from ,id_to, message)
        VALUES (:id_message, :id_from, :id_to, :message)
    ",
        $data
    );

    $results = DB::select('select * from messages where id_message = :id_message', [
        'id_message' => $data['id_message'],
    ]);
    return response()->json($results[0], 200);
});
