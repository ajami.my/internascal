<?php

Route::get('/users', function (Request $request) {
    $results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users ');
    return response()->json($results, 200);
});
Route::get('/users/roles/{role}', function ($role) {

    if (roleNoExists($role)) {
        abort(404);
    }

    $results = DB::select('select * from users where role=:role', [
        'role' => $role,
    ]);

    return response()->json($results, 200);
});

Route::get('/users/get-role/{iduser}', function ($iduser) {

    $results = DB::select('select role from users where iduser=:iduser', [
        'iduser' => $iduser,
    ]);

    return response()->json($results[0], 200);
});

Route::get('/soldVerefication/users/{idproduct}&{iduser}', function ($idproduct, $iduser) {

    if (userNoExists($iduser)) {
        abort(404);
    }

    $results = DB::select('select
     case
     when U.penascales >= (select price from products where idproduct=:idproduct)
     then 1 else 0
     end as posible
     from users U where iduser=:iduser', [
        'iduser' => $iduser,
        'idproduct' => $idproduct,
    ]);

    return response()->json($results[0], 200);
});

Route::get('/soldVerefication-for-promo/users/{idproduct}&{iduser}', function ($idproduct, $iduser) {

    if (userNoExists($iduser)) {
        abort(404);
    }

    $results = DB::select('select
     case
     when U.penascales >= (select new_price from products where idproduct=:idproduct)
     then 1 else 0
     end as posible
     from users U where iduser=:iduser', [
        'iduser' => $iduser,
        'idproduct' => $idproduct,
    ]);

    return response()->json($results[0], 200);
});
Route::get('/users/{iduser}', function ($iduser) {

    if (userNoExists($iduser)) {
        abort(404);
    }

    $results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users where iduser=:iduser ', [
        'iduser' => $iduser,
    ]);

    return response()->json($results[0], 200);
});

Route::post('/users', function () {

    $data = request()->all();

    // $numero = DB::select('select count(*) as numero from users where upper(mail)=upper(:mail)', [
    //    'mail' =>$data['mail'],
    // ]);
    $numero = DB::select('select * from users where upper(mail)=upper(:mail)', [
        'mail' => $data['mail'],
    ]);
    $listSize = sizeof($numero);

    if ($listSize == 0) {

        DB::insert(
            "
        INSERT into users (iduser, nickname, mail, password, datecreate)
        VALUES (:iduser, :nickname,:mail,  :password, :datecreate)
    ",
            $data
        );

        $results = [
            'succesful' => 'true',
        ];
        return response()->json($results, 200);
    } else {

        $results =

            [
            'succesful' => 'false',
        ];

        return response()->json($results, 200);

    }

});

Route::post('/log/users', function () {

    $data = request()->all();
    $mailExist = DB::select('select * from users where upper(mail)=upper(:mail)', [
        'mail' => $data['mail'],
    ]);
    $mailExist = sizeof($mailExist);
    if ($mailExist == 0) {
        $results = [
            'log_mensajes' => 'correo incorrecto',
        ];
        return response()->json($results, 200);
    } else {
        $passwordTrue = DB::select('select password from users where upper(mail)=upper(:mail)', [
            'mail' => $data['mail'],
        ]);
        $arr = [];
        foreach ($passwordTrue as $raw) {
            $arr[] = (array) $raw;
        }
        if ($data['password'] == $arr[0]['password']) {
            $results = DB::select('select iduser, role from users where upper(mail)=upper(:mail) ', [
                'mail' => $data['mail'],
            ]);
            return response()->json($results[0], 200);
        } else {
            $results =
                [
                'log_mensajes' => 'contraseña incorrecta',
            ];
        }

        return response()->json($results, 200);
    }
});




Route::PUT('/users/{iduser}', function (Request $request, $iduser) {
    $data = request()->all();

    if (userNoExists($iduser)) {
        abort(404);
    }

    DB::update("update users set  nickname=:nickname, role=:role,  penascales=:penascales,  urlphoto=:urlphoto     where iduser=:iduser",
        [
            'nickname' => $data['nickname'],
            'role' => $data['role'],
            'penascales' => $data['penascales'],
            'urlphoto' => $data['urlphoto'],
            'iduser' => $data['iduser'],
        ]
    );

    $results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users where iduser=:iduser', ['iduser' => $iduser]);
    return response()->json($results[0], 200);
});









Route::put('/users/updatePenascales/{idproduct}&{iduser}', function ($idproduct, $iduser) {
    $data = request()->all();
    DB::update('update users set penascales = penascales - (select price from products where idproduct=:idproduct) where iduser=:iduser', [
        'iduser' => $iduser,
        'idproduct' => $idproduct]);

    $results = [
        'succesful' => 'penascales updated',
    ];

    return response()->json($results, 200);
});

Route::put('/users/updatePenascales-after-reserving-promo/{idproduct}&{iduser}', function ($idproduct, $iduser) {
    $data = request()->all();
    DB::update('update users set penascales = penascales - (select new_price from products where idproduct=:idproduct) where iduser=:iduser', [
        'iduser' => $iduser,
        'idproduct' => $idproduct]);

    $results = [
        'succesful' => 'penascales updated',
    ];

    return response()->json($results, 200);
});

if (!function_exists('roleNoExists')) {
    function roleNoExists($role)
    {
        $results = DB::select('select * from users where role=:role', [
            'role' => $role,
        ]);
        return count($results) == 0;
    }
}
;
if (!function_exists('userNoExists')) {
    function userNoExists($iduser)
    {
        $results = DB::select('select * from users where iduser=:iduser', [
            'iduser' => $iduser,
        ]);
        return count($results) == 0;
    }
}
