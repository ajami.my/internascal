<?php

Route::get('/categories', function (Request $request) {
    $results = DB::select('select * from categories ORDER BY categoryName');
    return response()->json($results, 200);
});

Route::get('/get-categories-names', function (Request $request) {
    $results = DB::select('select categoryName from categories ORDER BY categoryName');
    return response()->json($results, 200);
});

Route::get('/categories/{idCategory}', function ($idCategory) {
    $results = DB::select('select * from categories where idCategory=:idCategory ', [
        'idCategory' => $idCategory,
    ]);
    return response()->json($results[0], 200);
});

Route::delete('/delete/categories/{idCategory}', function ($idCategory) {
    $data = request()->all();
    DB::delete('delete from categories where idCategory=:idCategory ', [
        'idCategory' => $idCategory,
    ]);

    $results = [
        'succesful' => 'category deleted',
    ];

    return response()->json($results, 200);
});

Route::post('/categories', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into categories (idCategory, categoryName, icons)
        VALUES (:idCategory, :categoryName, :icons)
    ",
        $data
    );
    $results = DB::select('select * from categories where idCategory = :idCategory', [
        'idCategory' => $data['idCategory'],
    ]);
    return response()->json($results[0], 200);
});

Route::PUT('/categories/{idCategory}', function (Request $request, $idCategory) {

    DB::update("update categories set  categoryName=?, icons=? where idCategory=?",
        [$request->categoryName,
            $request->icons,
            $request->idCategory,
        ]);

    $results = DB::select('select * from categories where idCategory = :idCategory', ['idCategory' => $idCategory]);
    return response()->json($results, 200);
});

if (!function_exists('categoryNoExists')) {
    function categoryNoExists($idCategory)
    {
        $results = DB::select('select * from categories where idCategory=:idCategory', [
            'idCategory' => $idCategory,
        ]);
        return count($results) == 0;
    }
}
