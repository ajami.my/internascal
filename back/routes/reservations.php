<?php

Route::get('/get-buyed-products/{id_user}', function ($id_user) {
    $results = DB::select('select P.*
    from reservations R
    join products P
    on R.idproduct = P.idproduct
    where P.state="Comprado"
    and R.iduser=:id_user
    ',
        [
            'id_user' => $id_user,
        ]
    );
    return response()->json($results, 200);
});
Route::get('/get-reserved-products/{id_user}', function ($id_user) {
    $results = DB::select('select P.*
    from reservations R
    join products P
    on R.idproduct = P.idproduct
    where P.state="Reservado"
    and R.iduser=:id_user
    ',
        [
            'id_user' => $id_user,
        ]
    );
    return response()->json($results, 200);
});

Route::post('/reservations', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into reservations (idReservation, idproduct, iduser)
        VALUES (:idReservation, :idproduct, :iduser)
    ",
        $data
    );

    $results = [
        'succesful' => true,
    ];
    return response()->json($results, 200);
});

Route::get('/reservations', function (Request $request) {
    $results = DB::select('select * from reservations');
    return response()->json($results, 200);
});

Route::get('/reservations/get-all-reservation-transactions', function (Request $request) {
    $results = DB::select('select R.idReservation,R.idproduct,R.iduser,U.iduser, U.nickname, P.name, P.price,P.urlphoto, p.state
    from reservations R
    left join users U on U.iduser=R.iduser
    left join products P on P.idproduct=R.idproduct
    where R.stat="Reservation"');
    return response()->json($results, 200);
});

Route::put('/reservations/devolverPenascales/{idproduct}', function ($idproduct) {
    $data = request()->all();
    DB::update('update users set penascales=penascales+ ( select price from products where idproduct=:idproduct)
     where iduser=(select iduser from reservations where idproduct=idproduct and state="Reservation")',
        [
            'idproduct' => $idproduct]);
    DB::update('update reservations set state ',
        [
            'idproduct' => $idproduct,
        ]);
    $results = [
        'succesful' => 'penascales returned',
    ];
    return response()->json($results, 200);
});
