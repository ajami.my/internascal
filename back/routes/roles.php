<?php

Route::get('/roles', function (Request $request) {
    $results = DB::select('select * from roles');
    return response()->json($results, 200);
});
Route::get('/roles/{idrole}', function ($idrole) {

    if (roleNoExists($idrole)) {
        abort(404);
    }

    $results = DB::select('select * from roles where idrole=:idrole ', [
        'idrole' => $idrole,
    ]);

    return response()->json($results[0], 200);
});
Route::post('/roles', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into roles (idrole, categoryName)
        VALUES (:idrole, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from roles where idrole = :idCategoidrolery', [
        'idrole' => $data['idrole'],
    ]);
    return response()->json($results[0], 200);
});
Route::put('/roles/{idrole}', function ($idrole) {
    if (roleNoExists($idrole)) {
        abort(404);
    }
    $data = request()->all();
    DB::delete(
        "
        delete from roles where idrole = :idrole",
        ['idrole' => $idrole]
    );
    DB::insert(
        "
        INSERT into roles (idrole, categoryName)
        VALUES (:idrole, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from roles where idrole = :idrole', ['idrole' => $idrole]);
    return response()->json($results[0], 200);
});

if (!function_exists('roleNoExists')) {
    function roleNoExists($idrole)
    {
        $results = DB::select('select * from roles where idrole=:idrole', [
            'idrole' => $idrole,
        ]);
        return count($results) == 0;
    }
}
